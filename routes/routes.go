package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/christopher.lim/appointment-backend/controllers"
	"gitlab.com/christopher.lim/appointment-backend/middleware"
)

func SetupRouter(router *gin.Engine) {

	// public routes
	router.POST("/v1/api/login", controllers.Login)

	// protected routes
	// user
	authenticated := router.Group("/")
	authenticated.Use(middleware.CheckAuth)
	authenticated.POST("/v1/api/register", controllers.Register)
	authenticated.GET("/v1/api/user/profile", controllers.GetProfile)
	authenticated.GET("/v1/api/user/:id", controllers.GetUser)
	authenticated.GET("/v1/api/user", controllers.GetAllUser)
	authenticated.PATCH("/v1/api/user/:id", controllers.UpdateUser)
	authenticated.DELETE("/v1/api/user/:id", controllers.RemoveUser)
	authenticated.GET("/v1/api/logout", controllers.UserLogout)

	//car
	authenticated.POST("/v1/api/car", controllers.AddCar)
	authenticated.GET("/v1/api/car/:id", controllers.GetCar)
	authenticated.GET("/v1/api/car", controllers.GetAllCar)
	authenticated.DELETE("/v1/api/car/:id", controllers.RemoveCar)

	//location
	authenticated.POST("/v1/api/location", controllers.AddLocation)
	authenticated.GET("/v1/api/location/:id", controllers.GetLocation)
	authenticated.GET("/v1/api/location", controllers.GetAllLocation)
	authenticated.DELETE("/v1/api/location/:id", controllers.RemoveLocation)

	//language
	authenticated.POST("/v1/api/language", controllers.AddLanguage)
	authenticated.GET("/v1/api/language/:id", controllers.GetLanguage)
	authenticated.GET("/v1/api/language", controllers.GetAllLanguage)
	authenticated.DELETE("/v1/api/language/:id", controllers.RemoveLanguage)

	//mechanic
	authenticated.POST("/v1/api/mechanic", controllers.AddMechanic)
	authenticated.GET("/v1/api/mechanic/:id", controllers.GetMechanic)
	authenticated.GET("/v1/api/mechanic", controllers.GetAllMechanic)
	authenticated.DELETE("/v1/api/mechanic/:id", controllers.RemoveMechanic)

	//client
	authenticated.POST("/v1/api/client", controllers.AddClient)
	authenticated.GET("/v1/api/client/:id", controllers.GetClient)
	authenticated.GET("/v1/api/client", controllers.GetAllClient)
	authenticated.PATCH("/v1/api/client/:id", controllers.UpdateClient)
	authenticated.DELETE("/v1/api/client/:id", controllers.RemoveClient)

	//service
	authenticated.POST("/v1/api/service", controllers.AddService)
	authenticated.GET("/v1/api/service/:id", controllers.GetService)
	authenticated.GET("/v1/api/service", controllers.GetAllService)
	authenticated.DELETE("/v1/api/service/:id", controllers.RemoveService)

	//appointment
	authenticated.POST("/v1/api/appointment", controllers.AddAppointment)
	authenticated.GET("/v1/api/appointment/:id", controllers.GetAppointment)
	authenticated.GET("/v1/api/appointment", controllers.GetAllAppointment)
	authenticated.PATCH("/v1/api/appointment/:id", controllers.UpdateAppointment)
	authenticated.DELETE("/v1/api/appointment/:id", controllers.RemoveAppointment)
}
