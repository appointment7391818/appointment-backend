package models

import "time"

type Appointment struct {
	ClientID    uint      `json:"client_id"`
	Client      Client    `gorm:"foreignkey:ClientID"`
	ID          uint      `json:"id" gorm:"primary_key"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	ServiceID   uint      `json:"service_id"`
	Service     Service   `gorm:"foreignkey:ServiceID"`
	StartDate   time.Time `json:"start_date"`
	EndDate     time.Time `json:"end_date"`
	LocationID  uint      `json:"location_id"`
	Location    Location  `gorm:"foreignkey:LocationID"`
	MechanicID  uint      `json:"mechanic_id"`
	Mechanic    Mechanic  `gorm:"foreignkey:MechanicID"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}
