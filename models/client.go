package models

import "time"

type Client struct {
	ID uint `json:"id" gorm:"primary_key"`
	Name string `json:"name"`
	Email string `json:"email" gorm:"unique"`
	Phone string `json:"phone"`
	ZipCode string `json:"zip_code"`
	CarID uint `json:"car_id"`
	Car Car `gorm:"foreignKey:CarID"`
	LocationID uint `json:"location_id"`
	Location Location `gorm:"foreignKey:LocationID"`
	CreatedAt time.Time
	UpdatedAt time.Time
}