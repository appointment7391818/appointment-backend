package models

type Mechanic struct {
	ID uint `json:"id" gorm:"primary_key"`
	FirstName string `json:"first_name"`
	LastName string `json:"last_name"`
	LocationID uint `json:"location_id"`
	Location   Location `gorm:"foreignKey:LocationID"`
}