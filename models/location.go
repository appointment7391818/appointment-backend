package models

type Location struct {
	ID uint `json:"id" gorm:"primary_key"`
	Name string `json:"name" gorm:"unique"`
}