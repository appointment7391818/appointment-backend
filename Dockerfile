# Start from the official Go image to build your application
FROM golang:latest

# Env variables
ENV PROJECT_DIR=/app \
    GO111MODULE=on \
    CGO_ENABLED=0

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy everything from the current directory to the PWD (Present Working Directory) inside the container
COPY . .

# Download all the dependencies
RUN go mod download

# Download and install air tool
RUN go install github.com/cosmtrek/air@latest

# This container exposes port 8080 to the outside world
EXPOSE 8080

# Use air to monitor changes in Go files and rebuild
ENTRYPOINT [ "air", "-c", "./air.toml" ]