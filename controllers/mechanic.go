package controllers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/christopher.lim/appointment-backend/database"
	"gitlab.com/christopher.lim/appointment-backend/models"
)

func AddMechanic(context *gin.Context) {
	var mechanic models.Mechanic

	if err := context.ShouldBindJSON(&mechanic); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}

	if mechanic.FirstName == "" || mechanic.LastName == "" || mechanic.LocationID == 0 {
		context.JSON(http.StatusBadRequest, gin.H{"error": "FirstName, LastName, and LocationID fields must not be empty"})
		return
	}

	mechanic = models.Mechanic{
		FirstName:  mechanic.FirstName,
		LastName:   mechanic.LastName,
		LocationID: mechanic.LocationID,
	}

	result := database.DB.Create(&mechanic)

	// Check if record creation was succesful
	if result.Error != nil {
		log.Printf("Error creating mechanic: %v", result.Error)
		return
	}
	
	if err := database.DB.Preload("Location").First(&mechanic, "id = ?", mechanic.ID).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Mechanic not found!"})
		return
	}

	context.JSON(http.StatusCreated, gin.H{"data": mechanic})
}

func GetMechanic(context *gin.Context) {
	id := context.Param("id")

	var mechanic models.Mechanic

	if err := database.DB.Preload("Location").First(&mechanic, "id = ?", id).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Mechanic not found!"})
		return
	}

	context.JSON(http.StatusCreated, gin.H{"data": mechanic})
}

func GetAllMechanic(context *gin.Context) {

	var mechanics []models.Mechanic

	if err := database.DB.Preload("Location").Find(&mechanics).Error; err != nil {
		log.Printf("Error while getting mechanics: %s", err.Error())

		context.JSON(http.StatusInternalServerError, gin.H{"error": "Cannot fetch mechanics"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": mechanics})
}

func RemoveMechanic(context *gin.Context) {
	id := context.Param("id")

	var mechanic models.Mechanic
	if err := database.DB.Where("Id = ?", id).First(&mechanic).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Mechanic not found"})
		return
	}

	database.DB.Delete(&mechanic)
	context.Status(http.StatusNoContent)
}
