package controllers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/christopher.lim/appointment-backend/database"
	"gitlab.com/christopher.lim/appointment-backend/models"
)

func AddAppointment(context *gin.Context) {
	var appointment models.Appointment

	if err := context.ShouldBindJSON(&appointment); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}

	if appointment.ClientID == 0 || appointment.Title == "" || appointment.Description == "" || appointment.ServiceID == 0 ||
		appointment.StartDate.IsZero() || appointment.EndDate.IsZero() || appointment.LocationID == 0 || appointment.MechanicID == 0 {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Fields must not be empty"})
		return
	}

	// Check for Similar Appointment with same Client
	var similarAppointment models.Appointment
	if err := database.DB.Where("client_id = ? AND start_date = ? AND end_date = ? AND id != ?",
		appointment.ClientID,
		appointment.StartDate,
		appointment.EndDate,
		appointment.ID).Find(&similarAppointment).Error; err != nil {

		context.JSON(http.StatusNotFound, gin.H{"error": "This Client has similar appointment!"})
		return
	}

	// checks if client exist
	var client models.Client
	if err := database.DB.First(&client, "id = ?", appointment.ClientID).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Client not found!"})
		return
	}

	// checks if service exist
	var service models.Client
	if err := database.DB.First(&service, "id = ?", appointment.ServiceID).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Service not found!"})
		return
	}

	// checks if location exist
	var location models.Location
	if err := database.DB.First(&location, "id = ?", appointment.LocationID).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Location not found!"})
		return
	}

	// checks if mechanic exist
	var mechanic models.Mechanic
	if err := database.DB.First(&mechanic, "id = ?", appointment.MechanicID).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Mechanic not found!"})
		return
	}

	appointment = models.Appointment{
		ClientID:    appointment.ClientID,
		Title:       appointment.Title,
		Description: appointment.Description,
		ServiceID:   appointment.ServiceID,
		StartDate:   appointment.StartDate,
		EndDate:     appointment.EndDate,
		LocationID:  appointment.LocationID,
		MechanicID:  appointment.MechanicID,
	}

	result := database.DB.Create(&appointment)

	// Check if record creation was succesful
	if result.Error != nil {
		log.Printf("Error creating appointment: %v", result.Error)
		return
	}

	if err := database.DB.Preload("Client").Preload("Service").Preload("Location").Preload("Mechanic").First(&appointment, "id = ?", appointment.ID).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Appointment not found!"})
		return
	}

	context.JSON(http.StatusCreated, gin.H{"data": appointment})
}

func GetAppointment(context *gin.Context) {
	id := context.Param("id")

	var appointment models.Appointment

	preLoad := database.DB.Preload("Client").Preload("Service").Preload("Location").Preload("Mechanic")
	if err := preLoad.First(&appointment, "id = ?", id).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Appointment not found!"})
		return
	}

	context.JSON(http.StatusCreated, gin.H{"data": appointment})
}

func GetAllAppointment(context *gin.Context) {
	var appointments []models.Appointment

	preLoad := database.DB.Preload("Client").Preload("Service").Preload("Location").Preload("Mechanic")
	if err := preLoad.Find(&appointments).Error; err != nil {
		log.Printf("Error while getting appointments: %s", err.Error())

		context.JSON(http.StatusInternalServerError, gin.H{"error": "Cannot fetch appointments"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": appointments})
}

func UpdateAppointment(context *gin.Context) {
	id := context.Param("id")
	var appointment models.Appointment

	preLoad := database.DB.Preload("Client").Preload("Service").Preload("Location").Preload("Mechanic")
	if err := preLoad.First(&appointment, "id = ?", id).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Appointment not found"})
		return
	}

	var updatedAppointmentInfo models.Appointment
	if err := context.BindJSON(&updatedAppointmentInfo); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid input data"})
		return
	}

	if updatedAppointmentInfo.ClientID == 0 || updatedAppointmentInfo.Title == "" || updatedAppointmentInfo.Description == "" || updatedAppointmentInfo.ServiceID == 0 ||
		updatedAppointmentInfo.StartDate.IsZero() || updatedAppointmentInfo.EndDate.IsZero() || updatedAppointmentInfo.LocationID == 0 || updatedAppointmentInfo.MechanicID == 0 {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Fields must not be empty"})
		return
	}

	// Check for Similar Appointment with same Client
	var similarAppointment models.Appointment
	if err := database.DB.Where("client_id = ? AND start_date = ? AND end_date = ? AND id != ?",
		appointment.ClientID,
		appointment.StartDate,
		appointment.EndDate,
		appointment.ID).Find(&similarAppointment).Error; err != nil {

		context.JSON(http.StatusNotFound, gin.H{"error": "This Client has similar appointment!"})
		return
	}

	// checks if client exist
	var client models.Client
	if err := database.DB.First(&client, "id = ?", updatedAppointmentInfo.ClientID).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Client not found!"})
		return
	}

	// checks if service exist
	var service models.Client
	if err := database.DB.First(&service, "id = ?", updatedAppointmentInfo.ServiceID).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Service not found!"})
		return
	}

	// checks if location exist
	var location models.Location
	if err := database.DB.First(&location, "id = ?", updatedAppointmentInfo.LocationID).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Location not found!"})
		return
	}

	// checks if mechanic exist
	var mechanic models.Mechanic
	if err := database.DB.First(&mechanic, "id = ?", updatedAppointmentInfo.MechanicID).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Mechanic not found!"})
		return
	}

	appointment.ClientID = updatedAppointmentInfo.ClientID
	appointment.Title = updatedAppointmentInfo.Title
	appointment.Description = updatedAppointmentInfo.Description
	appointment.ServiceID = updatedAppointmentInfo.ServiceID
	appointment.StartDate = updatedAppointmentInfo.StartDate
	appointment.EndDate = updatedAppointmentInfo.EndDate
	appointment.LocationID = updatedAppointmentInfo.LocationID
	appointment.MechanicID = updatedAppointmentInfo.MechanicID

	database.DB.Model(&appointment).Updates(appointment)

	reloads := database.DB.Preload("Client").Preload("Service").Preload("Location").Preload("Mechanic")
	if err := reloads.First(&appointment, "id = ?", id).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Appointment not found"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": appointment})
}

func RemoveAppointment(context *gin.Context) {
	id := context.Param("id")
	var appointment models.Appointment

	if err := database.DB.Where("Id = ?", id).First(&appointment).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Appointment not found"})
		return
	}

	database.DB.Delete(&appointment)
	context.Status(http.StatusNoContent)
}
