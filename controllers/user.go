package controllers

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/christopher.lim/appointment-backend/database"
	"gitlab.com/christopher.lim/appointment-backend/models"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

func Register(context *gin.Context) {
	var user models.User

	if err := context.ShouldBindJSON(&user); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}

	if user.Email == "" || user.Password == "" || user.FirstName == "" || user.LastName == "" {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Email, password, first name, and last name cannot be empty"})
		return
	}

	var userFound models.User
	database.DB.Where("email=?", user.Email).Find(&userFound)

	if userFound.ID != 0 {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Email already used"})
		return
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user = models.User{
		Email:     user.Email,
		Password:  string(passwordHash),
		FirstName: user.FirstName,
		LastName:  user.LastName,
	}

	database.DB.Create(&user)

	context.JSON(http.StatusCreated, gin.H{"data": user})
}

func Login(context *gin.Context) {

	var user models.User

	if err := context.ShouldBindJSON(&user); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}

	var userFound models.User
	database.DB.Where("email=?", user.Email).Find(&userFound)

	if userFound.ID == 0 {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Email not found"})
		return
	}

	if err := bcrypt.CompareHashAndPassword([]byte(userFound.Password), []byte(user.Password)); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "invalid password"})
		return
	}

	generateToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":  userFound.ID,
		"exp": time.Now().Add(time.Hour * 24).Unix(),
	})

	token, err := generateToken.SignedString([]byte(os.Getenv("SECRET")))

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Failed to generate token"})
	}

	context.JSON(http.StatusOK, gin.H{
		"token": token,
	})
}

func GetProfile(context *gin.Context) {
	user, _ := context.Get("currentUser")

	context.JSON(http.StatusOK, gin.H{
		"user": user,
	})
}

func GetUser(context *gin.Context) {
	id := context.Param("id")

	var user models.User

	if err := database.DB.Where("Id = ?", id).First(&user).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "User not found!"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": user})
}

func GetAllUser(context *gin.Context) {

	var users []models.User

	if err := database.DB.Find(&users).Error; err != nil {
		log.Printf("Error while getting users: %s", err.Error())

		context.JSON(http.StatusInternalServerError, gin.H{"error": "Cannot fetch users"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": users})
}

func UpdateUser(context *gin.Context) {
	id := context.Param("id")
	var user models.User

	if err := database.DB.Where("Id = ?", id).First(&user).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "User not found"})
		return
	}

	var updatedUserInfo models.User
	if err := context.BindJSON(&updatedUserInfo); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid input data"})
		return
	}

	if user.Email == "" || user.Password == "" || user.FirstName == "" || user.LastName == "" {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Email, password, first name, and last name cannot be empty"})
		return
	}

	var exists models.User
	if err := database.DB.Where("email = ? AND id <> ?", updatedUserInfo.Email, id).First(&exists).Error; err != gorm.ErrRecordNotFound {
		context.JSON(http.StatusConflict, gin.H{"error": "Email already in use"})
		return
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(updatedUserInfo.Password), bcrypt.DefaultCost)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user.Email = updatedUserInfo.Email
	user.Password = string(passwordHash)
	user.FirstName = updatedUserInfo.FirstName
	user.LastName = updatedUserInfo.LastName

	database.DB.Model(&user).Updates(user)
	context.JSON(http.StatusOK, gin.H{"data": user})
}

func RemoveUser(context *gin.Context) {
	id := context.Param("id")

	var user models.User
	if err := database.DB.Where("Id = ?", id).First(&user).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "User not found"})
		return
	}

	database.DB.Delete(&user)
	context.Status(http.StatusNoContent)
}

func UserLogout(context *gin.Context) {
	context.SetCookie("gin_cookie", "", -1, "", "", false, true)

	context.Redirect(http.StatusMovedPermanently, "/v1/api/login")
}
