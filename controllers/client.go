package controllers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/christopher.lim/appointment-backend/database"
	"gitlab.com/christopher.lim/appointment-backend/models"
	"gorm.io/gorm"
)

func AddClient(context *gin.Context) {
	var client models.Client

	if err := context.ShouldBindJSON(&client); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}

	if client.Name == "" || client.Email == "" || client.Phone == "" || client.ZipCode == "" || client.CarID == 0 || client.LocationID == 0 {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Name, Email, Phone, ZipCode, CardID and LocationID fields must not be empty"})
		return
	}

	client = models.Client{
		Name: client.Name,
		Email: client.Email,
		Phone: client.Phone,
		ZipCode: client.ZipCode,
		CarID: client.CarID,
		LocationID: client.LocationID,
	}

	result := database.DB.Create(&client)

	// Check if record creation was succesful
	if result.Error != nil {
		log.Printf("Error creating client: %v", result.Error)
		return
	}
	
	if err := database.DB.Preload("Location").Preload("Car").First(&client, "id = ?", client.ID).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Client not found!"})
		return
	}

	context.JSON(http.StatusCreated, gin.H{"data": client})
}


func GetClient(context *gin.Context) {
	id := context.Param("id")

	var client models.Client

	if err := database.DB.Preload("Location").Preload("Car").First(&client, "id = ?", id).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Client not found!"})
		return
	}

	context.JSON(http.StatusCreated, gin.H{"data": client})
}

func GetAllClient(context *gin.Context) {
	var clients []models.Client

	if err := database.DB.Preload("Location").Preload("Car").Find(&clients).Error; err != nil {
		log.Printf("Error while getting clients: %s", err.Error())

		context.JSON(http.StatusInternalServerError, gin.H{"error": "Cannot fetch clients"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": clients})
}

func UpdateClient(context *gin.Context) {
	id := context.Param("id")
	var client models.Client

	if err := database.DB.Preload("Location").Preload("Car").First(&client, "id = ?", id).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Client not found"})
		return
	}

	var updatedClientInfo models.Client
	if err := context.BindJSON(&updatedClientInfo); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid input data"})
		return
	}

	if updatedClientInfo.Name == "" || updatedClientInfo.Email == "" || updatedClientInfo.Phone == "" || updatedClientInfo.ZipCode == "" || updatedClientInfo.CarID == 0 || updatedClientInfo.LocationID == 0 {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Name, Email, Phone, ZipCode, CardID and LocationID fields must not be empty"})
		return
	}

	var exists models.Client
	if err := database.DB.Where("email = ? AND id <> ?", updatedClientInfo.Email, id).First(&exists).Error; err != gorm.ErrRecordNotFound {
		context.JSON(http.StatusConflict, gin.H{"error": "Email already in use"})
		return
	}

	client.Name = updatedClientInfo.Name
	client.Email = updatedClientInfo.Email
	client.Phone = updatedClientInfo.Phone
	client.ZipCode = updatedClientInfo.ZipCode
	client.CarID = updatedClientInfo.CarID
	client.LocationID = updatedClientInfo.LocationID
	
	database.DB.Model(&client).Updates(client)
	context.JSON(http.StatusOK, gin.H{"data": client})
}

func RemoveClient(context *gin.Context) {
	id := context.Param("id")

	var client models.Client
	if err := database.DB.Where("Id = ?", id).First(&client).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Client not found"})
		return
	}

	database.DB.Delete(&client)
	context.Status(http.StatusNoContent)
}
