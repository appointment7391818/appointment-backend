package controllers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/christopher.lim/appointment-backend/database"
	"gitlab.com/christopher.lim/appointment-backend/models"
)

func AddLanguage(context *gin.Context) {
	var language models.Language

	if err := context.ShouldBindJSON(&language); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}

	if language.Name == "" {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Language Name cannot be empty"})
		return
	}

	var languageFound models.Language
	database.DB.Where("name=?", language.Name).Find(&languageFound)

	if languageFound.ID != 0 {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Language Name already used"})
		return
	}

	language = models.Language{
		Name: language.Name,
	}

	database.DB.Create(&language)

	context.JSON(http.StatusCreated, gin.H{"data": language})
}

func GetLanguage(context *gin.Context) {
	id := context.Param("id")

	var language models.Language

	if err := database.DB.Where("Id = ?", id).First(&language).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Language not found!"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": language})

}

func GetAllLanguage(context *gin.Context) {

	var languages []models.Language

	if err := database.DB.Find(&languages).Error; err != nil {
		log.Printf("Error while getting languages: %s", err.Error())

		context.JSON(http.StatusInternalServerError, gin.H{"error": "Cannot fetch languages"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": languages})
}

func RemoveLanguage(context *gin.Context) {
	id := context.Param("id")

	var language models.Language
	if err := database.DB.Where("Id = ?", id).First(&language).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Language not found"})
		return
	}

	database.DB.Delete(&language)
	context.Status(http.StatusNoContent)
}
