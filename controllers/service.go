package controllers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/christopher.lim/appointment-backend/database"
	"gitlab.com/christopher.lim/appointment-backend/models"
)

func AddService(context *gin.Context) {
	var service models.Service

	if err := context.ShouldBindJSON(&service); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}

	if service.Name == "" {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Service Name cannot be empty"})
		return
	}

	var serviceFound models.Service
	database.DB.Where("name=?", service.Name).Find(&serviceFound)

	if serviceFound.ID != 0 {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Service Name already used"})
		return
	}

	service = models.Service{
		Name: service.Name,
	}

	database.DB.Create(&service)

	context.JSON(http.StatusCreated, gin.H{"data": service})
}

func GetService(context *gin.Context) {
	id := context.Param("id")
	var service models.Service

	if err := database.DB.Where("Id = ?", id).First(&service).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Service not found!"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": service})

}

func GetAllService(context *gin.Context) {
	var services []models.Service

	if err := database.DB.Find(&services).Error; err != nil {
		log.Printf("Error while getting services: %s", err.Error())

		context.JSON(http.StatusInternalServerError, gin.H{"error": "Cannot fetch services"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": services})
}

func RemoveService(context *gin.Context) {
	id := context.Param("id")
	var service models.Service

	if err := database.DB.Where("Id = ?", id).First(&service).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Service not found"})
		return
	}

	database.DB.Delete(&service)
	context.Status(http.StatusNoContent)
}
