package controllers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/christopher.lim/appointment-backend/database"
	"gitlab.com/christopher.lim/appointment-backend/models"
)



func AddCar(context *gin.Context) {
	var car models.Car

	if err := context.ShouldBindJSON(&car); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}

	if car.Name == "" {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Car Name cannot be empty"})
		return
	}

	var carFound models.Car
	database.DB.Where("name=?", car.Name).Find(&carFound)

	if carFound.ID != 0 {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Car Name already used"})
		return
	}

	car = models.Car{
		Name: car.Name,
	}

	database.DB.Create(&car)

	context.JSON(http.StatusCreated, gin.H{"data": car})
}

func GetCar(context *gin.Context) {
	id := context.Param("id")

	var car models.Car

	if err := database.DB.Where("Id = ?", id).First(&car).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Car not found!"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": car})

}

func GetAllCar(context *gin.Context) {

	var cars []models.Car

	if err := database.DB.Find(&cars).Error; err != nil {
		log.Printf("Error while getting cars: %s", err.Error())

		context.JSON(http.StatusInternalServerError, gin.H{"error": "Cannot fetch cars"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": cars})
}

func RemoveCar(context *gin.Context) {
	id := context.Param("id")

	var car models.Car
	if err := database.DB.Where("Id = ?", id).First(&car).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Car not found"})
		return
	}

	database.DB.Delete(&car)
	context.Status(http.StatusNoContent)
}