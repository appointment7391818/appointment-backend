package controllers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/christopher.lim/appointment-backend/database"
	"gitlab.com/christopher.lim/appointment-backend/models"
)



func AddLocation(context *gin.Context) {
	var location models.Location

	if err := context.ShouldBindJSON(&location); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request payload"})
		return
	}

	if location.Name == "" {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Location Name cannot be empty"})
		return
	}

	var locationFound models.Location
	database.DB.Where("name=?", location.Name).Find(&locationFound)

	if locationFound.ID != 0 {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Location Name already used"})
		return
	}

	location = models.Location{
		Name: location.Name,
	}

	database.DB.Create(&location)

	context.JSON(http.StatusCreated, gin.H{"data": location})
}

func GetLocation(context *gin.Context) {
	id := context.Param("id")

	var location models.Location

	if err := database.DB.Where("Id = ?", id).First(&location).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Location not found!"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": location})

}

func GetAllLocation(context *gin.Context) {

	var locations []models.Location

	if err := database.DB.Find(&locations).Error; err != nil {
		log.Printf("Error while getting locations: %s", err.Error())

		context.JSON(http.StatusInternalServerError, gin.H{"error": "Cannot fetch locations"})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": locations})
}

func RemoveLocation(context *gin.Context) {
	id := context.Param("id")

	var location models.Location
	if err := database.DB.Where("Id = ?", id).First(&location).Error; err != nil {
		context.JSON(http.StatusNotFound, gin.H{"error": "Location not found"})
		return
	}

	database.DB.Delete(&location)
	context.Status(http.StatusNoContent)
}