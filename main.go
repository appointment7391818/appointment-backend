package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/christopher.lim/appointment-backend/database"
	"gitlab.com/christopher.lim/appointment-backend/routes"
)

func main() {
	router := gin.Default()

	database.ConnectToDatabase()

	routes.SetupRouter(router)

	router.Run()
}
