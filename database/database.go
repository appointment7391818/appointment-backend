package database

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/christopher.lim/appointment-backend/models"
	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func ConnectToDatabase() {
	// Load .env file
	if err := godotenv.Load(); err != nil {
		log.Printf("No .env file found")
	}

	// Define a PostgreSQL connection
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true",
		os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"), os.Getenv("DB_NAME"))

	// Connect to PostgreSQL
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect to database: " + err.Error())
	}

	// Create table based on our Models
	err = db.AutoMigrate(&models.User{}, &models.Car{}, &models.Language{}, 
	&models.Location{}, &models.Mechanic{}, &models.Client{},
	&models.Service{}, &models.Appointment{})
	
	if err != nil {
		panic("failed to perform migrations: " + err.Error())
	}

	DB = db
}
