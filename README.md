# appointment-backend
This is the backend for the appointment web app that I will be creating using Go. It is a remake of the Capstone project I made for WGU.


## Installation
- Golang - https://go.dev/doc/install
- MySQL Workbench 8.0.36 - https://dev.mysql.com/downloads/workbench/
- Docker - https://www.docker.com/products/docker-desktop/

## Go Packages
```
go get github.com/gin-gonic/gin
go get gorm.io/gorm
go get -u github.com/go-sql-driver/mysql
go get github.com/joho/godotenv
go get github.com/golang-jwt/jwt/v4
go get golang.org/x/crypto
go install github.com/cosmtrek/air@latest
```

## Go Packages Description
```
Gin: A web framework written in Go and used to make API Calls.
Gorm: An object relational mapper for Go.
Mysql: A MySQL Driver for Go's database/sql.
Godotenv: Go dependency for managing variables.
Golang JWT: Go implementation of JSON Web Tokens.
Crypto: Go library used for password encryption and decryption.
Go Air: Go package for automatic app reloading upon saving changes.
```

## How to Run the Project
```
With Docker:
Start Docker
docker-compose up

Stop Docker
docker-compose down
```

```
Without Docker:
Just Go
go run .

With Go Air
air
```

```
sudo lsof -i -P -n | grep PORT NUMBER  
sudo kill -9 PID 
```